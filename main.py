import sys
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget, QInputDialog
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QTimer, QThread, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QMessageBox
from tools.ui_main import *
from tools.langs import langs
from random import randint
import json

class ThreadActivities(QThread):  # Receives frame
    def __init__(self):
        super().__init__()
        # Things to do on init

    def run(self):
        while self.running:
            # Things to do while looping
            pass

    def close(self):
        # Steps to close
        self.client.close()

class LangIt(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.learnedWords = []
        self.getData()
        self.currentWord = ""
        self.getWord()
        self.ui.word_btn.setText(self.currentWord)

        # Buttons
        self.ui.word_btn.pressed.connect(self.flipCard)
        self.ui.word_btn.released.connect(self.unflipCard)
        self.ui.yes_btn.clicked.connect(self.yesBtn)
        self.ui.no_btn.clicked.connect(self.noBtn)

    def getData(self):
        try:
            with open("LangIt/src/resources/learnedWords.json", "r") as fi:
                self.learnedWords = json.loads(fi.read())
        except:
            self.learnedWords = []

    def storeData(self):
        line = json.dumps(self.learnedWords, indent=2)
        with open("LangIt/src/resources/learnedWords.json", "w") as f:
            f.write(line)


    def getWord(self):
        self.currentWord = langs["french"][randint(0,len(langs["french"])-1)]
        return self.currentWord

    def flipCard(self):
        self.ui.word_btn.setText(langs["english"][langs["french"].index(self.currentWord)])
        

    def unflipCard(self):
        self.ui.word_btn.setText(self.currentWord)       

    def yesBtn(self):
        self.learnedWords.append(self.currentWord)
        self.getWord()
        if self.currentWord in self.learnedWords:
            while self.currentWord in self.learnedWords:
                self.getWord()
        self.ui.word_btn.setText(self.currentWord)
        self.storeData()

    def noBtn(self):
        self.getWord()
        if self.currentWord in self.learnedWords:
            while self.currentWord in self.learnedWords:
                self.getWord()
        self.ui.word_btn.setText(self.currentWord)
   

    def closeEvent(self, event):  # Close main window
        reply = QMessageBox.question(
            self, 'Close application', 'Are you sure you want to close the window?', QMessageBox.Yes | QMessageBox.No)
        if reply == QMessageBox.Yes:
            # Verificar que el dron esta en home y apagar todo
            event.accept()
        else:
            event.ignore()

def main_window():  # Run application
    app = QApplication(sys.argv)

    # create and show mainWindow
    mainWindow = LangIt()
    mainWindow.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main_window()
