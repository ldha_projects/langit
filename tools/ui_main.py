# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui_main.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(624, 600)
        MainWindow.setMinimumSize(QtCore.QSize(624, 600))
        MainWindow.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(100, 100, 103);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 0, 2, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem1, 0, 0, 1, 1)
        self.toolBox = QtWidgets.QToolBox(self.centralwidget)
        self.toolBox.setObjectName("toolBox")
        self.p1 = QtWidgets.QWidget()
        self.p1.setGeometry(QtCore.QRect(0, 0, 606, 502))
        self.p1.setObjectName("p1")
        self.gridLayout = QtWidgets.QGridLayout(self.p1)
        self.gridLayout.setContentsMargins(0, 0, 0, -1)
        self.gridLayout.setObjectName("gridLayout")
        self.no_btn = QtWidgets.QPushButton(self.p1)
        self.no_btn.setStyleSheet("font: 87 18pt \"Arial Black\";")
        self.no_btn.setObjectName("no_btn")
        self.gridLayout.addWidget(self.no_btn, 1, 0, 1, 1)
        self.yes_btn = QtWidgets.QPushButton(self.p1)
        self.yes_btn.setStyleSheet("font: 87 18pt \"Arial Black\";")
        self.yes_btn.setObjectName("yes_btn")
        self.gridLayout.addWidget(self.yes_btn, 1, 1, 1, 1)
        self.word_btn = QtWidgets.QPushButton(self.p1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.word_btn.sizePolicy().hasHeightForWidth())
        self.word_btn.setSizePolicy(sizePolicy)
        self.word_btn.setStyleSheet("background-color: rgba(147, 147, 147, 155);\n"
"border-radius:45px;\n"
"font: 87 36pt \"Arial Black\";")
        self.word_btn.setShortcut("")
        self.word_btn.setCheckable(False)
        self.word_btn.setObjectName("word_btn")
        self.gridLayout.addWidget(self.word_btn, 0, 0, 1, 2)
        self.toolBox.addItem(self.p1, "")
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setGeometry(QtCore.QRect(0, 0, 606, 502))
        self.page_2.setObjectName("page_2")
        self.toolBox.addItem(self.page_2, "")
        self.gridLayout_2.addWidget(self.toolBox, 2, 0, 1, 4)
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
        self.comboBox.setSizePolicy(sizePolicy)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.gridLayout_2.addWidget(self.comboBox, 0, 3, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem2, 0, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.toolBox.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.no_btn.setText(_translate("MainWindow", "No"))
        self.yes_btn.setText(_translate("MainWindow", "Yes"))
        self.word_btn.setText(_translate("MainWindow", "<------------------------------->"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.p1), _translate("MainWindow", "LangIt"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page_2), _translate("MainWindow", "Page 2"))
        self.comboBox.setItemText(0, _translate("MainWindow", "Spanish"))
        self.comboBox.setItemText(1, _translate("MainWindow", "French"))
        self.comboBox.setItemText(2, _translate("MainWindow", "Korean"))
        self.comboBox.setItemText(3, _translate("MainWindow", "Japanese"))
        self.comboBox.setItemText(4, _translate("MainWindow", "Mandarin"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
